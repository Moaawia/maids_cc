import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CacheService } from './cache.service';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private apiUrl = 'https://reqres.in/api';

  constructor(private http: HttpClient, private cacheService: CacheService) {}

  getUsers(page: number): Observable<any> {
    const cacheKey = `users_page_${page}`;

    // Check if data is already in the cache
    const cachedData = this.cacheService.get(cacheKey);
    if (cachedData) {
      // Serve data from the cache
      return new Observable((observer) => {
        observer.next(cachedData);
        observer.complete();
      });
    }

    // If not in cache, make the HTTP request
    const url = `${this.apiUrl}/users?page=${page}`;
    const request = this.http.get(url);

    // Cache the data for future use
    request.subscribe(
      (data) => this.cacheService.set(cacheKey, data),
      (error) => console.error('Error fetching users:', error)
    );

    return request;
  }
}